module.exports = {
  env: {
    'AMQP_URI': 'amqp://localhost:5672',
    'DEV_ENV': true,
    'GA_TRACKING_CODE': 'XX-000000000-0',
    'MONGO_URI': 'mongodb://localhost:27017/avbx',
    'PRIVATE_KEY_PATH': '/path/to/private/key',
    'PUBLIC_KEY_PATH': '/path/to/public/key',
    'PUSHER_KEY': '11111112222222333333',
    'PUSHER_CLUSTER': 'xx1',
    'REDIS_URI': 'redis://localhost:6379',
    'SANITY': true,
    'SENTRY_DSN': 'https://000@111.ingest.sentry.io/222',
    'SESSION_KEY': 'all that glitters isn\'t gold',
  }
}